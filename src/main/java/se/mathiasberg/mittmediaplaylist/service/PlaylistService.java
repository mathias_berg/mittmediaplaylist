package se.mathiasberg.mittmediaplaylist.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.mathiasberg.mittmediaplaylist.domain.Track;
import se.mathiasberg.mittmediaplaylist.entity.Playlist;
import se.mathiasberg.mittmediaplaylist.entity.PlaylistTrack;
import se.mathiasberg.mittmediaplaylist.repository.PlaylistRepository;
import se.mathiasberg.mittmediaplaylist.repository.PlaylistTrackRepository;

@Service
public class PlaylistService {
	private static final Logger log = LoggerFactory.getLogger(PlaylistService.class);
	
	@Autowired
	private PlaylistRepository playlistRepo;
	
	@Autowired
	private PlaylistTrackRepository playlistTrackRepo;
	
	public void create(Playlist playlist){
		log.info("create playlist " + playlist.getName());
		playlistRepo.save(playlist);
	}
	
	public void removeById(Integer id){
		log.info("removeById id " + id);
		playlistRepo.delete(id);
	}
	
	public void remove(Playlist playlist){
		log.info("remove playlist " + playlist.getName());
		playlistRepo.delete(playlist);
	}
	
	public void save(Playlist playlist){
		log.info("save playlist " + playlist.getName());
		playlistRepo.save(playlist);
	}
	
	public Playlist findPlaylist(Integer id){
		log.info("findPlaylist by id " + id);
		Playlist pl = playlistRepo.findOne(id);
		log.info("found one, or? " + (pl != null ? true :false));
		return pl;
	}
	
	public List<Playlist> findAllPlaylists(){
		log.info("findAllPlaylists ");
		Iterable<Playlist> it = playlistRepo.findAll();
		
		List<Playlist> playlists = new ArrayList<>();
		for (Playlist pl : it) {
			playlists.add(pl);
		}
		
		return playlists;
	}

	public void removeTrack(Integer id, Integer trackId) {
		Playlist pl = playlistRepo.findOne(id);
		
		for(PlaylistTrack track : pl.getTracks()){
			if(track.getId().equals(trackId)){
				pl.getTracks().remove(track);
			}
		}
		
	}

	public void addTrack(Integer id, Track track) {
		log.info("addTrack called ");
		Playlist pl = playlistRepo.findOne(id);
		PlaylistTrack t = new PlaylistTrack();
		t.setTrackId(track.getId());
		t.setName(track.getName());
		t.setPlaylist(pl);
		t.setDurationMs(track.getDuration_ms());
		t.setPreviewUrl(track.getPreview_url());
		
		pl.getTracks().add(t);
		//playlistRepo.save(pl);
		
		playlistTrackRepo.save(t);
		
		log.info(" === PlaylistTrack id created!! " + t.getId());
		
	}
}
