package se.mathiasberg.mittmediaplaylist.service;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


import se.mathiasberg.mittmediaplaylist.domain.Album;
import se.mathiasberg.mittmediaplaylist.domain.AlbumResult;
import se.mathiasberg.mittmediaplaylist.domain.AlbumTracksResult;
import se.mathiasberg.mittmediaplaylist.domain.Artist;
import se.mathiasberg.mittmediaplaylist.domain.ArtistsResult;
import se.mathiasberg.mittmediaplaylist.domain.ArtistsResultMapper;
import se.mathiasberg.mittmediaplaylist.domain.PageData;
import se.mathiasberg.mittmediaplaylist.domain.Track;
import se.mathiasberg.mittmediaplaylist.domain.mapper.BaseMapper;
import se.mathiasberg.mittmediaplaylist.http.client.HttpClient;

@Service
public class SpotifyApiService {
	
	private static final Logger log = LoggerFactory.getLogger(SpotifyApiService.class);
	
	/**
	 * Get all Tracks to an Artist
	 * @param artistId
	 * @return
	 * @throws Exception
	 */
	public List<Track> getTracksToArtist(String artistId) throws Exception {
		List<Album> albums = getAlbumsToArtist(artistId);
		List<Track> tracksToArtist = new ArrayList<>();
		for(Album album : albums){
			tracksToArtist.addAll(getTracksToAlbum(album.getId()));
		}
		
		return tracksToArtist;
	}
	
	public Artist getArtist(String artistId)throws Exception {
		//artists/65taSIk8y1qNzK8ddRewQe
		log.info("getArtist called, artistId " + artistId);
		HttpClient httpClient = HttpClient.createHttpClientBuilder("api.spotify.com", true)
				.build();
		PageData data = httpClient.executeGet("v1/artists/" + artistId);
		Artist artist = BaseMapper.create(data.getBody(), Artist.class);
		
		return artist;
	}
	
	/**
	 * Search Artist
	 * Calls to Spotify search REST 
	 */
	public List<Artist> searchArtists(String q) throws Exception {
		
		log.info("searchArtists called, query " + q);
		HttpClient httpClient = HttpClient.createHttpClientBuilder("api.spotify.com", true)
				.addParams("q", q)
				.addParams("limit", "5")
				.addParams("type", "artist")
				.build();
		PageData data = httpClient.executeGet("v1/search");
		ArtistsResult object = ArtistsResultMapper.create(data.getBody()).getArtists();
		Artist[] artists = object.getItems();
		
		return Arrays.asList(artists);
	}
	
	/**
	 * Fetch Albums to an artist.
	 * 
	 * @param artistId
	 * @return
	 */
	public List<Album> getAlbumsToArtist(String artistId)throws Exception {
		log.info("getAlbumsToArtist called , artistId " + artistId);
		HttpClient httpClient = HttpClient.createHttpClientBuilder("api.spotify.com", true)
				.addParams("album_type", "album,single")
				.build();
		PageData data = httpClient.executeGet("v1/artists/"+artistId+"/albums");
		Album[] albums = AlbumResult.create(data.getBody()).getItems();
		return Arrays.asList(albums);
	}
	
	/**
	 * Fetch Tracks to an album
	 * 
	 * @param albumId
	 * @return
	 * @throws Exception
	 */
	public List<Track> getTracksToAlbum(String albumId)throws Exception {
		log.info("getTracksToAlbum called");
		HttpClient httpClient = HttpClient.createHttpClientBuilder("api.spotify.com", true)
				//.addParams("album_type", "album,single")
				.build();
		PageData data = httpClient.executeGet("v1/albums/" + albumId + "/tracks");
		Track[] tracks = AlbumTracksResult.create(data.getBody()).getItems();
		log.info("getTracksToAlbum #tracks " + (tracks != null ? tracks.length : 0));
		
		return Arrays.asList(tracks);
	}

	public List<Track> getTracksToAlbums(Album album) throws Exception {
		List<Album> albums = new ArrayList<>();
		albums.add(album);
		return getTracksToAlbums(albums);
	}
	
	/**
	 * Get all Tracks to albums
	 * @param albums
	 * @return
	 * @throws Exception
	 */
	public List<Track> getTracksToAlbums(List<Album> albums) throws Exception {
		List<Track> tracksToAlbums = new ArrayList<>();
		for(Album a : albums){
			tracksToAlbums.addAll(getTracksToAlbum(a.getId()));
		}
		
		return tracksToAlbums;
	}
	
}
