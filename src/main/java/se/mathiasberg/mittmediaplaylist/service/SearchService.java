package se.mathiasberg.mittmediaplaylist.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.mathiasberg.mittmediaplaylist.domain.Artist;
import se.mathiasberg.mittmediaplaylist.domain.ArtistSearchResult;
import se.mathiasberg.mittmediaplaylist.domain.SearchResult;
import se.mathiasberg.mittmediaplaylist.domain.TracksResult;
import se.mathiasberg.mittmediaplaylist.repository.ArtistsRepository;

@Service
public class SearchService {

	private static final Logger log = LoggerFactory.getLogger(SearchService.class);

	@Autowired
	private ArtistsRepository artistsRepository;
	
	@Autowired
	private SpotifyApiService spotifyApiService;
	
	public SearchResult searchArtists(String q) throws Exception{
		log.info("searchArtists called : q=" + q);
		List<Artist> artists = artistsRepository.searchArtists(q);
		return new SearchResult(artists);
	}
	
	public TracksResult getTracksToArtist(Artist artist)throws Exception{
		log.info("getTracksToArtist called : artist id =" + artist.getId());
		
		ArtistSearchResult artistSearchResult = artistsRepository.getArtistSearchResult(artist);
		
		TracksResult result = new TracksResult();
		result.setArtistId(artist.getId());
		result.setArtistName(artist.getName());
		result.setArtistImage(artist.getImageSmall());
		result.setAlbumAndTracksList(artistSearchResult.getAlbumAndTracks());
		result.setTracks(artistSearchResult.getTracks());

		return result; 
	}
	
	public TracksResult getTracksToArtist(String artistId)throws Exception{
		log.info("getTracksToArtist called : artistId=" + artistId);
		Artist artist = spotifyApiService.getArtist(artistId);
		return getTracksToArtist(artist);
	}
	
}
