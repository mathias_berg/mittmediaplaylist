package se.mathiasberg.mittmediaplaylist.web;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import se.mathiasberg.mittmediaplaylist.domain.Artist;
import se.mathiasberg.mittmediaplaylist.domain.SearchResult;
import se.mathiasberg.mittmediaplaylist.domain.Track;
import se.mathiasberg.mittmediaplaylist.domain.TracksResult;
import se.mathiasberg.mittmediaplaylist.entity.Playlist;
import se.mathiasberg.mittmediaplaylist.service.PlaylistService;
import se.mathiasberg.mittmediaplaylist.service.SearchService;

@Controller
public class PlaylistController {

	private static final Logger log = LoggerFactory.getLogger(PlaylistController.class);
	
	@Autowired
	private PlaylistService playlistService;
	
	@Autowired
	private SearchService searchService;
	
	@RequestMapping(value = "/playlist", method = RequestMethod.GET)
	public ModelAndView getPlaylists() throws Exception{
		log.info("getPlaylists called : ");
		
		List<Playlist> playlists = playlistService.findAllPlaylists();
		ModelAndView mav = new ModelAndView();
		mav.setViewName("playlist");
		mav.addObject("playlists", playlists);
		mav.addObject("addTrackId", null);
		
		return mav;
	}
	
	@RequestMapping(value = "/playlist/{id}/tracks", method = RequestMethod.GET)
	public ModelAndView getPlaylistTracks(@PathVariable Integer id) throws Exception{
		log.info("getPlaylistTracks called id : " + id);
		
		Playlist playlist = playlistService.findPlaylist(id);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("playlistTracks");
		mav.addObject("playlist", playlist);
		
		return mav;
	}
	
	
	@RequestMapping(value = "/playlist/{id}}/track/{trackId}/remove", method = RequestMethod.GET)
	public ModelAndView removeTrackFromPlaylist(@PathVariable Integer id, @PathVariable Integer trackId) throws Exception{
		log.info("removeTrackFromPlaylist called id : " + id + " track :" + trackId);
		
		playlistService.removeTrack(id, trackId);
		
		
		return getPlaylistTracks(id);
	}
	
	@RequestMapping(value = "/playlist/create", method = RequestMethod.POST)
	public ModelAndView create(@RequestParam("name") String name) throws Exception{
		log.info("Create called : name=" + name);
		
		
		Playlist playlist = new Playlist();
		playlist.setName(name);
		playlistService.save(playlist);
		
		ModelAndView mav = getPlaylists();
		
		return mav;
	}
	
	@RequestMapping(value = "/playlist/delete/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable Integer id) throws Exception{
		log.info("delete called id : " + id);
		
		playlistService.removeById(id);
		
		return getPlaylists();
	}
	
	
	
	@RequestMapping(value = "/playlist/add/track/{artistId},{trackId}", method = RequestMethod.GET)
	public ModelAndView addTrackToSomePlaylist(@PathVariable String artistId, @PathVariable String trackId) throws Exception{
		log.info("addTrackToSomePlaylist called trackid : " + trackId + "artistid " + artistId);
		
		Track track = findTrackInfo(artistId, trackId);
		ModelAndView mav = getPlaylists();
		mav.addObject("addTrackToArtist", artistId);
		mav.addObject("addTrackId", (track != null ? track.getId() : ""));
		
		
		return mav;
	}

	
	
	@RequestMapping(value = "/playlist/{id}/add/track/{artistId},{trackId}", method = RequestMethod.GET)
	public ModelAndView addTrack(@PathVariable Integer id, @PathVariable String artistId, @PathVariable String trackId) throws Exception{
		log.info("addTrack called trackid : " + trackId + "artistid " + artistId + " playlist " + id);
		
		playlistService.addTrack(id, findTrackInfo(artistId, trackId));
		
		ModelAndView mav = getPlaylistTracks(id);
		
		return mav;
	}
	
	/**
	 * Find complete track info by artist id and track id
	 * If artistid has been called before, then the result will be cached, and track object can be fetched from this cache.
	 * 
	 * @param artistId
	 * @param trackId
	 * @return
	 * @throws Exception
	 */
	private Track findTrackInfo(String artistId, String trackId)
			throws Exception {
		Artist a = new Artist();
		a.setId(artistId);
		TracksResult tracksResult = searchService.getTracksToArtist(a);
		List<Track> artistTracks = tracksResult.getTracks();
		for(Track t : artistTracks){
			if(t.getId().equals(trackId)){
				return t;
			}
		}
		return null;
	}
	
}
