package se.mathiasberg.mittmediaplaylist.web;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import se.mathiasberg.mittmediaplaylist.domain.SearchResult;
import se.mathiasberg.mittmediaplaylist.domain.TracksResult;
import se.mathiasberg.mittmediaplaylist.service.SearchService;

@Controller
public class SearchController {

	private static final Logger log = LoggerFactory.getLogger(SearchController.class);
	
	@Autowired
	private SearchService searchService;
	
	
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public ModelAndView search(@RequestParam("q") String q) throws Exception{
		log.info("Search called : q=" + q);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("index");
		
		SearchResult searchResult = searchService.searchArtists(q);
		
		mav.addObject("searchResult", searchResult);
		
		return mav;
	}
	
	
	@RequestMapping(value = "/artist/{artistId}/tracks", method = RequestMethod.GET)
	public ModelAndView getArtistTracks(@PathVariable String artistId) throws Exception{
		log.info("getArtistTracks called : artistId=" + artistId);
		
		TracksResult tracksResult = searchService.getTracksToArtist(artistId);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("index");
		mav.addObject("tracksResult", tracksResult);
		mav.addObject("artistId", artistId);
		
		return mav;
	}
	
}
