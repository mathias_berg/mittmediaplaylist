package se.mathiasberg.mittmediaplaylist.http.client;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import se.mathiasberg.mittmediaplaylist.domain.PageData;



/**
 * 
 * @author mathias.berg
 *
 */


public class HttpClient {
	private final static Log log = LogFactory.getLog(HttpClient.class);
	
	private String accessPoint = null;
	
	private String host = null;
	private Map<String, String> httpHeaders = new HashMap<>();
	private Map<String, String> httpParams = new HashMap<>();
	
	private BasicCookieStore cookieStore = null; 
	private CloseableHttpClient httpclient = null;
	
	//https://api.linkedin.com/v1/people/id=68244870
	
	public static class HttpClientBuilder {
		private HttpClient mClient;
		public HttpClientBuilder(String accessPoint, String host){
			mClient = new HttpClient();
			mClient.setAccessPoint(accessPoint);
			mClient.setHost(host);
		}
		public HttpClientBuilder addHeader(String key, String value){
			mClient.getHeaders().put(key, value);
			return this;
		}
		public HttpClient build(){
			return mClient;
		}
		public HttpClientBuilder addParams(String name, String value) {
			mClient.getHttpParams().put(name, value);
			return this;
		}
		
	}
	
	public static HttpClientBuilder createHttpClientBuilder (String host, boolean useHttps){
		String protocol = "http://";
		if(useHttps){
			protocol = "https://";
		}
		return new HttpClientBuilder(protocol + host, host);
	}
	
	
	
	public HttpClient(){
		cookieStore = new BasicCookieStore();
		httpclient = HttpClients.custom()
				.setUserAgent("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.103 Safari/537.36")				
				.setDefaultCookieStore(cookieStore)
                .build();
	}
	
	public HttpClient(String accessPoint, String host){
		cookieStore = new BasicCookieStore();
		httpclient = HttpClients.custom()
				.setUserAgent("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.103 Safari/537.36")				
				.setDefaultCookieStore(cookieStore)
                .build();
		
		
		this.accessPoint = accessPoint;
		this.host = host;
	}
	
	private PageData consumeEntity(CloseableHttpResponse response){
		PageData data = new PageData();
		//System.out.println(" ==== response status:" + response.getStatusLine());
		data.setStatusLine(response.getStatusLine().toString());

		Header[] headers = response.getAllHeaders();
		for(Header header : headers)
		{
			log.debug(" ==== header " + header.toString());
		}

		//System.out.println(" ==== Initial set of cookies:");
		List<Cookie> cookies = cookieStore.getCookies();
		if (cookies.isEmpty()) {
			log.debug("None");
		} else {
			for (int i = 0; i < cookies.size(); i++) {
				log.debug("- " + cookies.get(i).toString());
			}
		}

		//System.out.println(" ==== response " + response.toString());

		HttpEntity entity = response.getEntity();
		if (entity != null) {
			try {
				data.setBody(EntityUtils.toString(entity));
				EntityUtils.consume(entity);
			} catch (IOException e) {
				log.error("Error consuming response",e);
			} 

		}
		return data;
	}
	
	public PageData executeGet(String actionUrl)throws Exception{
		return executeGet(actionUrl, httpHeaders, httpParams);
	}
	
	public PageData executeGet(String query, Map<String, String> headers, Map<String, String> params) throws Exception{
		
		List <NameValuePair> nvps = new ArrayList <NameValuePair>();
		StringBuilder queryStr = new StringBuilder();
		queryStr.append(accessPoint).append("/").append(query).append("?");
		StringBuilder paramStr = new StringBuilder();
		if(params != null && params.size()>0){
			for(Map.Entry<String, String> entry : params.entrySet()){
				if(paramStr.length() > 0){
					paramStr.append("&");
				}
				paramStr.append(entry.getKey())
				.append("=")
				.append(entry.getValue());
				
				nvps.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
			}			
		}
		queryStr.append(paramStr.toString());
		log.debug("query " + queryStr.toString());
		log.debug("nvps " + nvps.toString());
		
		HttpGet httpget = new HttpGet(queryStr.toString());
		        
        httpget.addHeader("Host", host);
       
        if(headers != null && headers.size()>0){
			for(Map.Entry<String, String> entry : headers.entrySet()){
				httpget.addHeader(entry.getKey(), entry.getValue());
			}			
		}
        
       
        
        CloseableHttpResponse response = httpclient.execute(httpget);
        try{
        	
        	return  consumeEntity(response);
        	
        }finally {
        	response.close();
        }
	}
	
	public PageData executePost(String query)throws Exception{
		return executePost(query, httpParams, httpHeaders);
	}
	
	public PageData executePost(String query, Map<String, String> params, Map<String, String> headers)throws Exception{
		
		HttpPost httpPost = new HttpPost(accessPoint+ "/" + query);
		//httpPost.addHeader("Referer", accessPoint);
		//httpPost.addHeader("Origin", origin);
		httpPost.addHeader("Host", host);
		//httpPost.addHeader("Accept", "*/*");
		if(headers != null && headers.size()>0){
			for(Map.Entry<String, String> entry : headers.entrySet()){
				httpPost.addHeader(entry.getKey(), entry.getValue());
			}			
		}
		
		List <NameValuePair> nvps = new ArrayList <NameValuePair>();
		if(params != null && params.size()>0){
			for(Map.Entry<String, String> entry : params.entrySet()){
				nvps.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
			}			
		}
		
		httpPost.setEntity(new UrlEncodedFormEntity(nvps));
		CloseableHttpResponse response = httpclient.execute(httpPost);
        
        PageData data = new PageData();
        try{
        	data = consumeEntity(response);
        }finally {
        	response.close();
        }
		return data;
	}
	
	public PageData executePost(String actionUrl, Map<String, String> params) throws Exception{
		log.info(" ######### executePost " );
		
		RequestBuilder builder = RequestBuilder.post()
        .setUri(new URI(accessPoint + actionUrl));
		if(params.size()>0){
			for(Map.Entry<String, String> entry : params.entrySet()){
				builder.addParameter(entry.getKey(), entry.getValue());
			}			
		}
		
		HttpUriRequest login = builder
			//.addHeader("Referer", accessPoint)
			//.addHeader("Origin", origin)
			.addHeader("Host", host)
			.build();
		
        CloseableHttpResponse response = httpclient.execute(login);
        
        PageData data = new PageData();
        try{
        	data = consumeEntity(response);
        }finally {
        	response.close();
        }
		return data;
	}
	
	
	
	public void close() throws Exception{
		httpclient.close();
	}

	public Map<String, String> getHeaders() {
		return httpHeaders;
	}

	public void setHeaders(Map<String, String> headers) {
		this.httpHeaders = headers;
	}

	public String getAccessPoint() {
		return accessPoint;
	}

	public void setAccessPoint(String accessPoint) {
		this.accessPoint = accessPoint;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Map<String, String> getHttpParams() {
		return httpParams;
	}

	public void setHttpParams(Map<String, String> httpParams) {
		this.httpParams = httpParams;
	}
	
	
	

}
