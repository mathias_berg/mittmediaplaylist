package se.mathiasberg.mittmediaplaylist.domain;

import se.mathiasberg.mittmediaplaylist.domain.mapper.BaseMapper;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ArtistsResultMapper extends BaseMapper{

	private ArtistsResult artists;

	public ArtistsResult getArtists() {
		return artists;
	}

	public void setArtists(ArtistsResult artists) {
		this.artists = artists;
	}
	
	public static ArtistsResult createArtists(String jsonResult) throws Exception{
		ObjectMapper mapper = new ObjectMapper();
		ArtistsResult object = mapper.readValue(jsonResult, ArtistsResult.class);
		return object;
	}
	
	public static ArtistsResultMapper create(String jsonResult) throws Exception{
		ObjectMapper mapper = new ObjectMapper();
		ArtistsResultMapper object = mapper.readValue(jsonResult, ArtistsResultMapper.class);
		return object;
	}
}
