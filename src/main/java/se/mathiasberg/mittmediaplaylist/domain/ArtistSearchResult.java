package se.mathiasberg.mittmediaplaylist.domain;

import java.util.ArrayList;
import java.util.List;

public class ArtistSearchResult {

	private Artist artist;
	private List<Album> albums = new ArrayList<>();
	private List<Track> tracks = new ArrayList<>();
	private List<AlbumAndTrack> albumAndTracks = new ArrayList<>();
	
	public Artist getArtist() {
		return artist;
	}
	public void setArtist(Artist artist) {
		this.artist = artist;
	}
	public List<Album> getAlbums() {
		return albums;
	}
	public void setAlbums(List<Album> albums) {
		this.albums = albums;
	}
	public List<Track> getTracks() {
		return tracks;
	}
	public void setTracks(List<Track> tracks) {
		this.tracks = tracks;
	}
	
	public void add(Album album, List<Track> tracks) {
		
		for(Track t : tracks){
			AlbumAndTrack at = new AlbumAndTrack(album, t);
			albumAndTracks.add(at);
		}
		
		this.albums.add(album);
		this.tracks.addAll(tracks);
		
	}
	
	
	public List<AlbumAndTrack> getAlbumAndTracks() {
		return albumAndTracks;
	}
	public void setAlbumAndTracks(List<AlbumAndTrack> albumAndTracks) {
		this.albumAndTracks = albumAndTracks;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((albumAndTracks == null) ? 0 : albumAndTracks.hashCode());
		result = prime * result + ((albums == null) ? 0 : albums.hashCode());
		result = prime * result + ((artist == null) ? 0 : artist.hashCode());
		result = prime * result + ((tracks == null) ? 0 : tracks.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArtistSearchResult other = (ArtistSearchResult) obj;
		if (albumAndTracks == null) {
			if (other.albumAndTracks != null)
				return false;
		} else if (!albumAndTracks.equals(other.albumAndTracks))
			return false;
		if (albums == null) {
			if (other.albums != null)
				return false;
		} else if (!albums.equals(other.albums))
			return false;
		if (artist == null) {
			if (other.artist != null)
				return false;
		} else if (!artist.equals(other.artist))
			return false;
		if (tracks == null) {
			if (other.tracks != null)
				return false;
		} else if (!tracks.equals(other.tracks))
			return false;
		return true;
	}
	
	
}
