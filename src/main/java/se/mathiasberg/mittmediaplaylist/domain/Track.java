package se.mathiasberg.mittmediaplaylist.domain;

import java.util.Arrays;

public class Track {
	private Artist[] artists;
	private String[] available_markets;
	private int disc_number;
	private long duration_ms;
	private boolean explicit;
	private ExternalUrls external_urls;
	private String href;
	private String id;
	private String name;
	private String preview_url;
	private String track_number;
	private String type;
	private String uri;
	
	public Artist[] getArtists() {
		return artists;
	}
	public void setArtists(Artist[] artists) {
		this.artists = artists;
	}
	public String[] getAvailable_markets() {
		return available_markets;
	}
	public void setAvailable_markets(String[] available_markets) {
		this.available_markets = available_markets;
	}
	public int getDisc_number() {
		return disc_number;
	}
	public void setDisc_number(int disc_number) {
		this.disc_number = disc_number;
	}
	public long getDuration_ms() {
		return duration_ms;
	}
	public void setDuration_ms(long duration_ms) {
		this.duration_ms = duration_ms;
	}
	public boolean isExplicit() {
		return explicit;
	}
	public void setExplicit(boolean explicit) {
		this.explicit = explicit;
	}
	public ExternalUrls getExternal_urls() {
		return external_urls;
	}
	public void setExternal_urls(ExternalUrls external_urls) {
		this.external_urls = external_urls;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPreview_url() {
		return preview_url;
	}
	public void setPreview_url(String preview_url) {
		this.preview_url = preview_url;
	}
	public String getTrack_number() {
		return track_number;
	}
	public void setTrack_number(String track_number) {
		this.track_number = track_number;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	@Override
	public String toString() {
		return "Track [available_markets=" + Arrays.toString(available_markets)
				+ ", disc_number=" + disc_number + ", duration_ms="
				+ duration_ms + ", explicit=" + explicit + ", external_urls="
				+ external_urls + ", href=" + href + ", id=" + id + ", name="
				+ name + ", preview_url=" + preview_url + ", track_number="
				+ track_number + ", type=" + type + ", uri=" + uri +  " artists=" + Arrays.toString(artists)
				+ "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(artists);
		result = prime * result + Arrays.hashCode(available_markets);
		result = prime * result + disc_number;
		result = prime * result + (int) (duration_ms ^ (duration_ms >>> 32));
		result = prime * result + (explicit ? 1231 : 1237);
		result = prime * result
				+ ((external_urls == null) ? 0 : external_urls.hashCode());
		result = prime * result + ((href == null) ? 0 : href.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((preview_url == null) ? 0 : preview_url.hashCode());
		result = prime * result
				+ ((track_number == null) ? 0 : track_number.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Track other = (Track) obj;
		if (!Arrays.equals(artists, other.artists))
			return false;
		if (!Arrays.equals(available_markets, other.available_markets))
			return false;
		if (disc_number != other.disc_number)
			return false;
		if (duration_ms != other.duration_ms)
			return false;
		if (explicit != other.explicit)
			return false;
		if (external_urls == null) {
			if (other.external_urls != null)
				return false;
		} else if (!external_urls.equals(other.external_urls))
			return false;
		if (href == null) {
			if (other.href != null)
				return false;
		} else if (!href.equals(other.href))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (preview_url == null) {
			if (other.preview_url != null)
				return false;
		} else if (!preview_url.equals(other.preview_url))
			return false;
		if (track_number == null) {
			if (other.track_number != null)
				return false;
		} else if (!track_number.equals(other.track_number))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (uri == null) {
			if (other.uri != null)
				return false;
		} else if (!uri.equals(other.uri))
			return false;
		return true;
	}
	
	
	
}
