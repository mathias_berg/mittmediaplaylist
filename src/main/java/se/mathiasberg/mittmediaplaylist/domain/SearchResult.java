package se.mathiasberg.mittmediaplaylist.domain;

import java.util.List;

public class SearchResult {
	
	private List<Artist> artists;
	
	
	public SearchResult(List<Artist> artists) {
		this.artists = artists;
	}

	public List<Artist> getArtists() {
		return artists;
	}

	public void setArtists(List<Artist> artists) {
		this.artists = artists;
	}

	

}
