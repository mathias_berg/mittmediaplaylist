package se.mathiasberg.mittmediaplaylist.domain;

import java.util.List;


public class TracksResult {

	private String artistName;
	private String artistId;
	private Image artistImage;
	private Image albumImage;
	private List<AlbumAndTrack> albumAndTracksList;
	
	private List<Track> tracks;

	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	public String getArtistId() {
		return artistId;
	}

	public void setArtistId(String artistId) {
		this.artistId = artistId;
	}

	

	public Image getArtistImage() {
		return artistImage;
	}

	public void setArtistImage(Image artistImage) {
		this.artistImage = artistImage;
	}

	public Image getAlbumImage() {
		return albumImage;
	}

	public void setAlbumImage(Image albumImage) {
		this.albumImage = albumImage;
	}

	public List<Track> getTracks() {
		return tracks;
	}

	public void setTracks(List<Track> tracks) {
		this.tracks = tracks;
	}

	public List<AlbumAndTrack> getAlbumAndTracksList() {
		return albumAndTracksList;
	}

	public void setAlbumAndTracksList(List<AlbumAndTrack> albumAndTracksList) {
		this.albumAndTracksList = albumAndTracksList;
	}

	

	
	
	
	
	
}
