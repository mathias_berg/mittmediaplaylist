package se.mathiasberg.mittmediaplaylist.domain;


public class ExternalUrls {
	public String spotify;

	@Override
	public String toString() {
		return "ExternalUrls [spotify=" + spotify + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((spotify == null) ? 0 : spotify.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExternalUrls other = (ExternalUrls) obj;
		if (spotify == null) {
			if (other.spotify != null)
				return false;
		} else if (!spotify.equals(other.spotify))
			return false;
		return true;
	}

	
	
	
}
