package se.mathiasberg.mittmediaplaylist.domain;

import java.util.Arrays;

public class Album extends BaseDomain {

	private String album_type;
	private String[] available_markets;
	private ExternalUrls external_urls;	
	private String href;
	private String id;
	private String name;
	private String type;
	private String uri;
	public String getAlbum_type() {
		return album_type;
	}
	public void setAlbum_type(String album_type) {
		this.album_type = album_type;
	}
	public String[] getAvailable_markets() {
		return available_markets;
	}
	public void setAvailable_markets(String[] available_markets) {
		this.available_markets = available_markets;
	}
	public ExternalUrls getExternal_urls() {
		return external_urls;
	}
	public void setExternal_urls(ExternalUrls external_urls) {
		this.external_urls = external_urls;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((album_type == null) ? 0 : album_type.hashCode());
		result = prime * result + Arrays.hashCode(available_markets);
		result = prime * result
				+ ((external_urls == null) ? 0 : external_urls.hashCode());
		result = prime * result + ((href == null) ? 0 : href.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + Arrays.hashCode(images);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Album other = (Album) obj;
		if (album_type == null) {
			if (other.album_type != null)
				return false;
		} else if (!album_type.equals(other.album_type))
			return false;
		if (!Arrays.equals(available_markets, other.available_markets))
			return false;
		if (external_urls == null) {
			if (other.external_urls != null)
				return false;
		} else if (!external_urls.equals(other.external_urls))
			return false;
		if (href == null) {
			if (other.href != null)
				return false;
		} else if (!href.equals(other.href))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (!Arrays.equals(images, other.images))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (uri == null) {
			if (other.uri != null)
				return false;
		} else if (!uri.equals(other.uri))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Album [album_type=" + album_type + ", available_markets="
				+ Arrays.toString(available_markets) + ", external_urls="
				+ external_urls + ", href=" + href + ", id=" + id + ", images="
				+ Arrays.toString(images) + ", name=" + name + ", type=" + type
				+ ", uri=" + uri + "]";
	}
	
	
}
