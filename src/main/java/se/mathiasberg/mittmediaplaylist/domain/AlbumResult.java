package se.mathiasberg.mittmediaplaylist.domain;

import java.util.Arrays;

import com.fasterxml.jackson.databind.ObjectMapper;

public class AlbumResult {

	public String href;
	public Album[] items;
	public int limit;
	public String next;
	public int offset;
	public String previous;
	public int total;
	
	public static AlbumResult create(String jsonResult) throws Exception{
		ObjectMapper mapper = new ObjectMapper();
		AlbumResult object = mapper.readValue(jsonResult, AlbumResult.class);
		return object;
	}

	
	
	public Album[] getItems() {
		return items;
	}



	public void setItems(Album[] items) {
		this.items = items;
	}



	@Override
	public String toString() {
		return "AlbumResult [href=" + href + ", items="
				+ Arrays.toString(items) + ", limit=" + limit + ", next="
				+ next + ", offset=" + offset + ", previous=" + previous
				+ ", total=" + total + "]";
	}

	
	
	
}
