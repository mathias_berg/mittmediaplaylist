package se.mathiasberg.mittmediaplaylist.domain;


public class Followers {
	public String href;
	public int total;
	@Override
	public String toString() {
		return "Followers [href=" + href + ", total=" + total + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((href == null) ? 0 : href.hashCode());
		result = prime * result + total;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Followers other = (Followers) obj;
		if (href == null) {
			if (other.href != null)
				return false;
		} else if (!href.equals(other.href))
			return false;
		if (total != other.total)
			return false;
		return true;
	}
	
	
	
	
}
