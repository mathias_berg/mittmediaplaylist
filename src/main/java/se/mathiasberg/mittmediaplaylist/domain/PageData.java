package se.mathiasberg.mittmediaplaylist.domain;

public class PageData {

	private String statusLine;
	private String body;
	
	
	
	public String getBody(){
		return body;
	}
	
	public void setBody(String body) {
		this.body = body;
		
	}

	public String getStatusLine() {
		return statusLine;
	}

	public void setStatusLine(String statusLine) {
		this.statusLine = statusLine;
	}
	
	

}
