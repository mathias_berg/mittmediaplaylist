package se.mathiasberg.mittmediaplaylist.domain.mapper;


import com.fasterxml.jackson.databind.ObjectMapper;

public class BaseMapper {
	
	public static <T> T create(String jsonObject, Class<T> valueType)throws Exception{
		ObjectMapper mapper = new ObjectMapper();
		T object = mapper.readValue(jsonObject, valueType);
		return object;
	}

}
