package se.mathiasberg.mittmediaplaylist.domain;

public class BaseDomain {
	protected Image[] images = new Image[]{};
	
	public Image[] getImages() {
		return images;
	}

	public void setImages(Image[] images) {
		this.images = images;
	}
	
	public Image getImageLarge(){
		for(Image im : images){
			if(im.getHeight() <= 640 && im.getHeight() >300){
				return im;
			}
		}
		return new Image();
	}
	
	public Image getImageMedium(){
		for(Image im : images){
			if(im.getHeight() <= 300 && im.getHeight() >64){
				return im;
			}
		}
		return new Image();
	}
	
	public Image getImageSmall(){
		for(Image im : images){
			if(im.getHeight() <= 64){
				return im;
			}
		}
		return new Image();
	}
	
}
