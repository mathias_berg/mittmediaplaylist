package se.mathiasberg.mittmediaplaylist.domain;

import com.fasterxml.jackson.databind.ObjectMapper;

public class AlbumTracksResult {

	public String href;
	public Track[] items;
	public int limit;
	public String next;
	public int offset;
	public String previous;
	public int total;
	
	
	
	public Track[] getItems() {
		return items;
	}



	public void setItems(Track[] items) {
		this.items = items;
	}



	public static AlbumTracksResult create(String jsonResult) throws Exception{
		ObjectMapper mapper = new ObjectMapper();
		AlbumTracksResult object = mapper.readValue(jsonResult, AlbumTracksResult.class);
		return object;
	}
	
}
