package se.mathiasberg.mittmediaplaylist.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class PlaylistTrack implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	private String trackId;
	private String externalId;
	private String name;
	private String previewUrl;
	private String uri;
	private long durationMs;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Playlist playlist;
	
	public Playlist getPlaylist() {
		return playlist;
	}
	public void setPlaylist(Playlist playlist) {
		this.playlist = playlist;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPreviewUrl() {
		return previewUrl;
	}
	public void setPreviewUrl(String previewUrl) {
		this.previewUrl = previewUrl;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public long getDurationMs() {
		return durationMs;
	}
	public void setDurationMs(long durationMs) {
		this.durationMs = durationMs;
	}
	
	
	
	public String getTrackId() {
		return trackId;
	}
	public void setTrackId(String trackId) {
		this.trackId = trackId;
	}
	@Override
	public String toString() {
		return "PlaylistTrack [id=" + id + ", externalId=" + externalId
				+ ", name=" + name + ", previewUrl=" + previewUrl + ", uri="
				+ uri + ", durationMs=" + durationMs + "]";
	}
	
	
	
}
