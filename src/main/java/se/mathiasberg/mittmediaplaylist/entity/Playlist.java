package se.mathiasberg.mittmediaplaylist.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cascade;

@Entity
public class Playlist implements java.io.Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer playlistId;
	private String name;
	
	@OneToMany(fetch = FetchType.EAGER,cascade=CascadeType.ALL, mappedBy = "playlist")
	private List<PlaylistTrack> tracks = new ArrayList<>();;

	public Integer getPlaylistId() {
		return playlistId;
	}


	public void setPlaylistId(Integer playlistId) {
		this.playlistId = playlistId;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public List<PlaylistTrack> getTracks() {
		return tracks;
	}


	public void setTracks(List<PlaylistTrack> tracks) {
		this.tracks = tracks;
	}


	@Override
	public String toString() {
		return "Playlist [playlistId=" + playlistId + ", name=" + name
				+ ", tracks=" + tracks + "]";
	}


	
	
	
	

}
