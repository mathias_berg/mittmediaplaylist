package se.mathiasberg.mittmediaplaylist;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import se.mathiasberg.mittmediaplaylist.repository.ArtistsRepository;


@SpringBootApplication
@EnableCaching
public class MittMediaPlaylistApplication {

	private static final Logger log = LoggerFactory.getLogger(MittMediaPlaylistApplication.class);
	
	@Component
    static class Runner implements CommandLineRunner {
        @Autowired
        private ArtistsRepository artistsRepository;

        @Override
        public void run(String... args) throws Exception {
            log.info(".... Fetching artists");
            /*log.info("isbn-1234 -->" + artistsRepository.getById("isbn-1234"));
            log.info("isbn-4567 -->" + artistsRepository.getById("isbn-4567"));
            log.info("isbn-1234 -->" + artistsRepository.getById("isbn-1234"));
            log.info("isbn-4567 -->" + artistsRepository.getById("isbn-4567"));
            log.info("isbn-1234 -->" + artistsRepository.getById("isbn-1234"));
            log.info("isbn-1234 -->" + artistsRepository.getById("isbn-1234"));*/
        }
    }

    @Bean
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager("ArtistSearchResult");
    }
	
	public static void main(String[] args) {
		SpringApplication.run(MittMediaPlaylistApplication.class, args);

	}

}
