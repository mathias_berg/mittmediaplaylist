package se.mathiasberg.mittmediaplaylist.repository;

import java.util.List;

import se.mathiasberg.mittmediaplaylist.domain.Artist;
import se.mathiasberg.mittmediaplaylist.domain.ArtistSearchResult;

public interface ArtistsRepository {
	List<Artist> searchArtists(String q) throws Exception;
	
	ArtistSearchResult getArtistSearchResult(Artist artist) throws Exception;
	
}
