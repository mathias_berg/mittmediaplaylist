package se.mathiasberg.mittmediaplaylist.repository;

import org.springframework.data.repository.CrudRepository;

import se.mathiasberg.mittmediaplaylist.entity.PlaylistTrack;

public interface PlaylistTrackRepository extends CrudRepository<PlaylistTrack, Integer> { 

}
