package se.mathiasberg.mittmediaplaylist.repository;



import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import se.mathiasberg.mittmediaplaylist.domain.Album;
import se.mathiasberg.mittmediaplaylist.domain.Artist;
import se.mathiasberg.mittmediaplaylist.domain.ArtistSearchResult;
import se.mathiasberg.mittmediaplaylist.domain.Track;
import se.mathiasberg.mittmediaplaylist.service.SpotifyApiService;

@Component
public class ArtistsRepositoryImpl implements ArtistsRepository {

	private static final Logger log = LoggerFactory.getLogger(ArtistsRepositoryImpl.class);
	
	@Autowired
	private SpotifyApiService spotifyApiService;
	


	/*@Override
	//@Cacheable("ArtistSearchResult")
	public List<ArtistSearchResult> searchArtists(String q) throws Exception {
		log.info("### searchArtists called , query " + q);
		List<Artist> artists = spotifyApiService.searchArtists(q);
		log.info("Search hits " + artists.size());
		List<ArtistSearchResult> resultList = new ArrayList<>();
		for(Artist a : artists){
			log.info("### searchArtists, artistId " + a.getId());
			ArtistSearchResult result = new ArtistSearchResult();
			result.setArtist(a);
			List<Album> albums = spotifyApiService.getAlbumsToArtist(a.getId());
			List<Track> tracks = spotifyApiService.getTracksToAlbums(albums);
			result.setAlbums(albums);
			result.setTracks(tracks);
			//ArtistSearchResult result = getArtistSearchResult(a.getId());
			//result.setArtist(a);
			resultList.add(result);
		}
		
		return resultList;
		
	}*/
	@Override
	public List<Artist> searchArtists(String q) throws Exception {
		log.info("### searchArtists called , query " + q);
		List<Artist> artists = spotifyApiService.searchArtists(q);
		log.info("Search hits " + artists.size());
		
		
		return artists;
		
	}
	
	
	
	@Override
	@Cacheable(value="ArtistSearchResult", key="#artist.id")
	public ArtistSearchResult getArtistSearchResult(Artist artist)
			throws Exception {
		log.info("@@@ getArtistSearchResult called, artistId " + artist.getId());
		ArtistSearchResult result = new ArtistSearchResult();
		result.setArtist(artist);
		List<Album> albums = spotifyApiService.getAlbumsToArtist(artist.getId());
		for(Album album : albums){
			List<Track> tracks = spotifyApiService.getTracksToAlbums(album);
			result.add(album, tracks);
		}
		
		return result;
	}


	//@Cacheable("ArtistSearchResult")
	/*public ArtistSearchResult getArtistSearchResult(String artistId) throws Exception {
		log.info("@@@ getArtistSearchResult called, artistId " + artistId);
		ArtistSearchResult result = new ArtistSearchResult();
		
		List<Album> albums = spotifyApiService.getAlbumsToArtist(artistId);
		List<Track> tracks = spotifyApiService.getTracksToAlbums(albums);
		result.setAlbums(albums);
		result.setTracks(tracks);
		
		return result;
	}*/
	
	
	 // Don't do this at home
    private void simulateSlowService() {
        try {
            long time = 5000L;
            Thread.sleep(time);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }

}
