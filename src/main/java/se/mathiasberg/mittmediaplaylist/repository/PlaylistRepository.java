package se.mathiasberg.mittmediaplaylist.repository;

import org.springframework.data.repository.CrudRepository;

import se.mathiasberg.mittmediaplaylist.entity.Playlist;


public interface PlaylistRepository extends CrudRepository<Playlist, Integer> {

}
