package se.mathiasberg.mittmediaplaylist.http.client;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import se.mathiasberg.mittmediaplaylist.domain.Album;
import se.mathiasberg.mittmediaplaylist.domain.AlbumResult;
import se.mathiasberg.mittmediaplaylist.domain.AlbumTracksResult;
import se.mathiasberg.mittmediaplaylist.domain.Artist;
import se.mathiasberg.mittmediaplaylist.domain.ArtistsResult;
import se.mathiasberg.mittmediaplaylist.domain.ArtistsResultMapper;
import se.mathiasberg.mittmediaplaylist.domain.PageData;
import se.mathiasberg.mittmediaplaylist.domain.Track;

public class HttpClientTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testCallSearch() throws Exception {
		//https://api.spotify.com/v1/search?q=tania%20bowra&type=artist
		HttpClient httpClient = HttpClient.createHttpClientBuilder("api.spotify.com", true)
				.addParams("q", "tania")
				.addParams("type", "artist")
				.build();
		PageData data = httpClient.executeGet("v1/search");
		System.out.println(data.getBody());
		assertNotNull(data);
	}
	
	@Test
	///v1/albums/1u25SFaYWOCt4cUJfcXqAJ/tracks
	public void testGetAlbumTracks()throws Exception {
		HttpClient httpClient = HttpClient.createHttpClientBuilder("api.spotify.com", true)
				//.addParams("album_type", "album,single")
				.build();
		PageData data = httpClient.executeGet("v1/albums/1u25SFaYWOCt4cUJfcXqAJ/tracks");
		
		assertNotNull(data);
		System.out.println(data.getBody());
		
		Track[] tracks = AlbumTracksResult.create(data.getBody()).getItems();
		System.out.println(Arrays.toString(tracks));
		
	}
	
	@Test
	//v1/artists/65taSIk8y1qNzK8ddRewQe/albums?album_type=album,single
	public void testGetArtistAlbums()throws Exception {
		HttpClient httpClient = HttpClient.createHttpClientBuilder("api.spotify.com", true)
				.addParams("album_type", "album,single")
				.build();
		PageData data = httpClient.executeGet("v1/artists/65taSIk8y1qNzK8ddRewQe/albums");
		
		assertNotNull(data);
		System.out.println(data.getBody());
		
		Album[] albums = AlbumResult.create(data.getBody()).getItems();
		System.out.println(Arrays.toString(albums));
	}
	
	@Test
	public void testSearchArtistJsonToObject()throws Exception {
		
		HttpClient httpClient = HttpClient.createHttpClientBuilder("api.spotify.com", true)
				.addParams("q", "tania")
				.addParams("type", "artist")
				.build();
		PageData data = httpClient.executeGet("v1/search");
		assertNotNull(data);
		System.out.println(data.getBody());
		
		//ObjectMapper mapper = new ObjectMapper();
		//Artist[] object = mapper.readValue(data.getBody(), Artist[].class);
		ArtistsResult object = ArtistsResultMapper.create(data.getBody()).getArtists();
		
		System.out.println(object);
		assertNotNull(object);
		
	}

}
