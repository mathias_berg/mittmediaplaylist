package se.mathiasberg.mittmediaplaylist.repository;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import se.mathiasberg.mittmediaplaylist.MittMediaPlaylistApplication;
import se.mathiasberg.mittmediaplaylist.domain.Artist;
import se.mathiasberg.mittmediaplaylist.domain.ArtistSearchResult;
import se.mathiasberg.mittmediaplaylist.repository.ArtistsRepository;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MittMediaPlaylistApplication.class)
public class ArtistsRepoImplTest {

	private static final Logger log = LoggerFactory.getLogger(ArtistsRepoImplTest.class);
	
	@Autowired
    private ArtistsRepository artistsRepository;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testArtistsRepositoryInit() {
		assertNotNull(artistsRepository);
	}
	
	

	@Test
	public void testCacheSearchArtists() throws Exception{
		log.info(".... Fetching SearchArtists");
		
		List<Artist> artists = artistsRepository.searchArtists("tania");
		for(Artist a : artists){
			ArtistSearchResult result = artistsRepository.getArtistSearchResult(a);
			log.info("======= ");
			ArtistSearchResult result2 = artistsRepository.getArtistSearchResult(a);
			//System.out.println("Result " + result);
			//System.out.println("Result2 " + result2);
			assertEquals(result.toString(), result2.toString());
		}
		
		log.info("######## ");
        
	}
	
}
