package se.mathiasberg.mittmediaplaylist.service;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import se.mathiasberg.mittmediaplaylist.MittMediaPlaylistApplication;
import se.mathiasberg.mittmediaplaylist.entity.Playlist;
import se.mathiasberg.mittmediaplaylist.entity.PlaylistTrack;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MittMediaPlaylistApplication.class)
public class PlaylistServiceTest {
	
	@Autowired
	private PlaylistService playlistService;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}
	
	@Test
	public void testCreatePlaylist(){
		
		Playlist pl = new Playlist();
		pl.setName("Test");
		//pl.setTracks(tracks);
		playlistService.save(pl);
		assertNotNull(pl.getPlaylistId());
		
		List<PlaylistTrack> tracks = new ArrayList<PlaylistTrack>();
		PlaylistTrack track = new PlaylistTrack();
		track.setName("Track1");
		track.setPlaylist(pl);
		tracks.add(track);
		pl.setTracks(tracks);
		
		playlistService.save(pl);
		
		List<Playlist> it = playlistService.findAllPlaylists();
		assertNotNull(it);
		
		System.out.println("playlist " + it.toString());
		
	}
}
