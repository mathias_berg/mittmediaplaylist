package se.mathiasberg.mittmediaplaylist.service;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import se.mathiasberg.mittmediaplaylist.MittMediaPlaylistApplication;
import se.mathiasberg.mittmediaplaylist.domain.Artist;
import se.mathiasberg.mittmediaplaylist.repository.ArtistsRepositoryImpl;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MittMediaPlaylistApplication.class)
@ContextConfiguration(classes = {SearchService.class, ArtistsRepositoryImpl.class, SpotifyApiService.class})
public class SearchServiceTest {

	@Autowired
	SearchService service;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() throws Exception {
		Artist a = new Artist();
		a.setId("65taSIk8y1qNzK8ddRewQe");
		service.searchArtists("tania");
		service.getTracksToArtist(a);
		service.getTracksToArtist(a);
		System.out.println("");
		
	}

}
