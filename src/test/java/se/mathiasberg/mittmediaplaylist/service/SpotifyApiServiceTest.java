package se.mathiasberg.mittmediaplaylist.service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import se.mathiasberg.mittmediaplaylist.domain.Artist;

@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
@ContextConfiguration(classes = {SpotifyApiService.class})
public class SpotifyApiServiceTest {

	@Autowired
	SpotifyApiService service;
	
	@Test
	public void testGetArtistById() throws Exception{
		
		Artist a = service.getArtist("65taSIk8y1qNzK8ddRewQe");
		
		assertNotNull(a);
		
		System.out.println(a);
	}
	
	
	
}
