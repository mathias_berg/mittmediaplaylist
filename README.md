# README #

== Playlist App


With Playlist App you can search tracks on Spotify and add this to your own playlists.

Playlist App is build using JEE and Spring framework Web.
Also Api calls to Spotify is cached using Spring framework caching.
Playlist is stored on a inmemory database (h2database)
Playlist App is running on Spring framework Boot, wich is a framework to to package a embedded webb application server.


Build the project by running gradle command
gradlew.bat build

You need Java SDK configured on your machine to make gradle build.

Run Playlist App by this java command (Java setup is needed)
java -jar build\libs\MittMediaPlaylist-0.0.1-SNAPSHOT.jar

This will boot up a embedded tomcat webb application server, by Spring framework boot.
You can reach the webb start page by this url
http://localhost:9999/


Playlist App webb application, contains on to functions
 - Search
 - Playlist
 
 You find Artist on search page. And by clicking on "Låtar" link, you can see its tracks.
 Tracks can be added to a playlist, by clicking on "Lägg till" link.
 When clicking on "Lägg till" you will come to playlist page. Choose the playlist you want to add the track to, by clicking on "Lägg till låt".
 Playlist is created and viewed on playlist page.
 On playlist page you can se your playlist and click down to se your tracks to the playlist.
 Clicking on "lyssna" on the playlist page, will redirect you to spotity page, where you can hear a preview on the track.
 
